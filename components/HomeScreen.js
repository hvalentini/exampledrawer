import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import { Router } from '../Router.js';

export default class HomeScreen extends Component {
  /**
    * This is where we can define any route configuration for this
    * screen. For example, in addition to the navigationBar title we
    * could add backgroundColor.
    */
  static route = {
    navigationBar: {
      renderTitle: () => <Text>Algar Telecom @@</Text>,
      renderRight: () => <Text>OI</Text>,
    },
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Home Screennnnnnn !!!! :D</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
  },
});
